from django.views.decorators.http import require_http_methods
from .models import Shoe, BinVO
# from wardrobe.api.wardrobe_api.models import BinVO
from django.http import JsonResponse
import json
from common.json import ModelEncoder
from .acls import get_photo


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href", "closet_name", "id"
    ]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["id", "model_name", "color", "manufacturer", "bin"]
    # def get_extra_data(self, o):
    #     return {"bin": o.bin.bin_name}
    encoders = {
        "bin": BinVODetailEncoder(),
    }

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }

# Create your views here.
@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
        # if bin_vo_id is not None:
        #     shoes = Shoe.objects.filter(bin=bin_vo_id)
        # else:
        #     shoes = Shoe.objects.all()
        # return JsonResponse(
        #     {"shoes": shoes},
        #     encoder=ShoeListEncoder,
        #)
    else:
        content = json.loads(request.body)
        # shoe = Shoe.objects.create(**content)
        try:
            # bin = BinVO.objects.get(import_href=content["bin"])
            # bin_href = content['bin']
            # bin_href = f'/api/bins/{content["bin"]}/'
            # bin = BinVO.objects.get(import_href=bin_href)
            # content['bin'] = bin

            #code below is some testing about how the POST
            #is attached to the bin
            bin_id = content['bin']
            # print({"bin_id is": bin_id})
            # print({"content is": content})
            bin = BinVO.objects.get(id=bin_id)
            content['bin'] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin"},
                status=400,
            )
        # content = json.loads(request.body)
        photo = get_photo(
            content["color"],
            content["manufacturer"],
            content["model_name"])
        content.update(photo)
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_shoe(request, pk):
    try:
        shoe = Shoe.objects.get(id=pk)
    except Shoe.DoesNotExist:
        return JsonResponse(
            {"message": "This shoe does not exist"},
            status=400,
            )
    if request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
            )
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Shoe.objections.filter(id=pk).update(**content)
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
