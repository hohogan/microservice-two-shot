from django.db import models
from django.urls import reverse
# Create your models here.
# class Bin(models.Model):
#     pass

class BinVO(models.Model):
    # import_href = models.CharField(max_length=100, unique=True, null=True)
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()

    # def __str__(self):
    #     return self.closet_name
    import_href = models.CharField(max_length=100, unique=True, null=True)
    # name = models.CharField(max_length=200, null=True)
class Shoe(models.Model):
    manufacturer = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=20)
    picture_url = models.URLField(null=True)
    bin = models.ForeignKey(
        BinVO,
        related_name="shoe",
        on_delete=models.CASCADE,
        null=True
    )
    def get_api_url(self):
        return reverse("showShoes", kwargs={"pk": self.pk})

# from django.db import models

# # Create your models here.


# class BinVO(models.Model):
#     closet_name = models.CharField(max_length=100)
#     bin_number = models.PositiveSmallIntegerField()
#     bin_size = models.PositiveSmallIntegerField()
#     import_href = models.CharField(
#         max_length=200,
#         unique=True,
#         null=True,
#     )

#     def __str__(self):
#         return self.closet_name

#     class Meta:
#         ordering = (
#             "closet_name",
#             "bin_number",
#             "bin_size",
#             "import_href"
#             )


# class Shoe(models.Model):
#     model_name = models.CharField(max_length=100)
#     manufacturer = models.CharField(max_length=100)
#     color = models.CharField(max_length=100)
#     url = models.URLField(null=True)

#     bin = models.ForeignKey(
#         BinVO,
#         on_delete=models.CASCADE,
#         related_name='shoes',
#         null=True,
#     )

#     def __str__(self):
#         return self.model_name

#     class Meta:
#         ordering = (
#             "model_name",
#             "manufacturer",
#             "color",
#             "url",
#             )
