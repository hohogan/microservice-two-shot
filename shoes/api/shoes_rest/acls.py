from .keys import PEXELS_API_KEY
import json
import requests
# import os
# import sys
# sys.path.append("")
# PEXELS = os.environ["PEXELS_API_KEY"]


# # from shoes.gitignore import PEXELS_API_KEY

# #the code below is if i routed the pexels key in through the .env
# # PEXELS_API_KEY = os.environ["PEXELS_API_KEY"]

def get_photo(color, manufacturer, model_name):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": f"{color} {manufacturer} {model_name} shoes",
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}
