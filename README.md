# Wardrobify

Team:

* Jonathan Ceasar Medina - Shoes
* Mike - Hats

## Design

Wardrobify is an app to keep track of your shoes and hats.

The approach to this was to work with the given models from the wardrobe program and app. Then create a workflow
to run our respective apps to the wardrobe. We mapped out how we were going to intergrate and design the applications together.

## Shoes microservice

The wardrobe has a one-to-many relationship with the bins which hold the shoes.
The bins have a one-to-many relationship with the shoes and there can be many shoes.
The hats microservice's models has a BinVO (Bin Value Object) that acts as a way to connect Bins within the wardrobe and "hold"
shoes within it.
Shoes have the qualities of manufacturer, model name, color, and the bin they are assigned to.
Upon creation, the shoes are assigned a photo url retrieved from a 3rd party application, PEXELS.

The current project contains the minimum viable project as outlined by the rubric and
holds capability for more front-end development in terms of making it look nicer for a user experience.

## Hats microservice

Hats microservice will allow the user to add, list, and delete hat entries. The user can create hats
with values that must contain, style, color, fabric, and a location for them to set them in (in a
location value object) and an image will be automatically generated for them through pexels api. The interface
will be interactive for them, through restful API and polling will refresh the inventory from the wardrobe microservice every minute.
