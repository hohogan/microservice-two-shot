import React, { useState, useEffect } from 'react';

function ShoeForm() {
    const [modelName, setModelName] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [color, setColor] = useState('');
    const [bin, setBin] = useState('');
    const [bins, setBins] = useState([]);

    async function fetchBins() {
        const url = 'http://localhost:8100/api/bins/';

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log(data);
            setBins(data.bins);
        }
    }

    useEffect(() => {
        fetchBins();
    }, [])

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            "model_name": modelName,
            "manufacturer": manufacturer,
            "color": color,
            "bin": bin,
        }
        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'applications/json',
            }
        };
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);
            setModelName('');
            setManufacturer('');
            setColor('');
            setBin('');
        }
    }
    function handleChangeModelName(event) {
        const { value } = event.target;
        setModelName(value);
    }
    function handleChangeManufacturer(event) {
        const { value } = event.target;
        setManufacturer(value);
    }
    function handleChangeColor(event) {
        const { value } = event.target;
        setColor(value);
    }
    function handleChangeBin(event) {
        const { value } = event.target;
        setBin(value);
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1> Add a new shoe </h1>
                    <form onSubmit={handleSubmit} id="create-shoe-form">
                        <div className="form-floating mb-3">
                            <input value={modelName} onChange={handleChangeModelName} placeholder="Model Name" required type="text" name="Model Name" id="Model Name" className="form-control"/>
                            <label htmlFor="Model Name"> Model Name </label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={manufacturer} onChange={handleChangeManufacturer} placeholder="Manufacturer" required type="text" name="Manufacturer" id="Manufacturer" className="form-control"/>
                            <label htmlFor="Manufacturer"> Manufacturer </label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={color} onChange={handleChangeColor} placeholder="Color" required type="text" name="Color" id="Color" className="form-control"/>
                            <label htmlFor="Color"> Color </label>
                        </div>
                        <div className="mb-3">
                            <select value={bin} onChange={handleChangeBin} required name="bin" id="Bin" className="form-select">
                            <option value=""> Choose a bin </option>
                            {bins.map((bin) => {
                                return (
                                    <option key={bin.id} value={bin.id}> {bin.closet_name} </option>
                                )
                            })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ShoeForm;
