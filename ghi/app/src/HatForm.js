import React from 'react';

class HatForm extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            name: '',
            fabric: '',
            style: '',
            color: '',
            location: '',
            locations: [],
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handleStyleChange = this.handleStyleChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.locations

        const hatsUrl = 'http://localhost:8090/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(hatsUrl, fetchConfig);
        console.log(response);

        if (response.ok) {
            const newHat = await response.json();


            const cleared = {
                name: '',
                fabric: '',
                style: '',
                color: '',
                location: '',
            };

            this.setState(cleared);
        }
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value })
    }

    handleFabricChange(event) {
        const value = event.target.value;
        this.setState({ fabric: value })
    }

    handleStyleChange(event) {
        const value = event.target.value;
        this.setState({ style: value })
    }

    handleColorChange(event) {
        const value = event.target.value;
        this.setState({ color: value })
    }

    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({ location: value })
    }



    async componentDidMount() {
        const url = "http://localhost:8100/api/locations/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({ locations: data.locations });
        }
    }

    render() {
        return (
            <>
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="text-center shadow p-4 mt-4">
                            <h1>Create a new Hat</h1>
                            <form onSubmit={this.handleSubmit} id="create-hat-form">
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleNameChange} value={this.state.name} placeholder="name" required
                                        type="text" name="name" id="name"
                                        className="form-control" />
                                    <label htmlFor="name">Name</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleFabricChange} value={this.state.fabric} placeholder="fabric" required
                                        type="text" name="fabric" id="fabric"
                                        className="form-control" />
                                    <label htmlFor="fabric">Fabric</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleStyleChange} value={this.state.style} placeholder="Style" required type="text" name="style" id="style" className="form-control" />
                                    <label htmlFor="style">Style</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleColorChange} value={this.state.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                                    <label htmlFor="color">Color</label>
                                </div>
                                <div className="mb-3">
                                    <select onChange={this.handleLocationChange} value={this.state.location} required id="location" name="location" className="form-select">
                                        <option value="">Choose a location</option>
                                        {this.state.locations.map(location => {
                                            return (
                                                <option key={location.id} value={location.href}>{location.closet_name} Closet (sec: {location.section_number} shelf: {location.shelf_number})</option>
                                            );
                                        })}
                                    </select>
                                </div>
                                <button className="btn btn-primary">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default HatForm;
