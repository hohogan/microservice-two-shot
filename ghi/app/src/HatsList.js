import React from 'react';
import { Link } from "react-router-dom";

const deleteHat = async (id) => {
    fetch(`http://localhost:8090/hats/${id}/`, {
        method: "DELETE",
        headers: {
            'Content-Type': 'application/json'
        }
    })
}

function HatsColumns(props) {
    return (
        <div className="col">
            {props.list.map(data => {
                const hat = data;
                return (
                    <div>
                        <div key={hat.id} className="card mb-3 shadow">
                            <img src={hat.url} className="card-img-top" alt=""></img>
                            <h5 className="card-header">{hat.name}
                            <h6 className="ccard-subtitle mb-2 text-muted">{hat.color} {hat.style} hat</h6></h5>
                            <h6 className="card-body mb-2 text-muted">
                                Closet: {hat.location.closet_name} <br></br>Section: {hat.location.section_number} / Shelf: {hat.location.shelf_number}
                            </h6>
                            <h6><button onClick={() => deleteHat(hat.id)} type="button" className="btn btn-danger">
                                Delete
                            </button></h6>
                        </div>
                    </div>
                );
            })}
        </div>
    );
}

class HatsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hatsColumns: [[], [], [], []]
        };
    }

    async componentDidMount() {
        const url = 'http://localhost:8090/hats/';

        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();

                const requests = [];
                for (let hat of data.hats) {
                    const detailUrl = `http://localhost:8090/hats/${hat.id}`;
                    requests.push(fetch(detailUrl));
                }

                const responses = await Promise.all(requests);

                const hatsColumns = [[], [], [], []]

                let i = 0;
                for (const hatResponse of responses) {
                    if (hatResponse.ok) {
                        const details = await hatResponse.json();
                        hatsColumns[i].push(details)
                        i += 1;
                        if (i > 3) {
                            i = 0;
                        }
                    } else {
                        console.error(hatResponse);
                    }
                }

                this.setState({ hatsColumns: hatsColumns })
            }
        } catch (e) {
            console.error(e);
        }
    }

    render() {
        return (
            <>
                <h1 className="center">Here are your hats:</h1>
                <div className="text-center bg-info">
                    <div className="col-lg-6 mx-auto">
                    </div>
                </div>
                <div className="container text-center">
                    <br></br>
                    <div className="row">
                        {this.state.hatsColumns.map((list, index) => {
                            return (
                                <HatsColumns key={index} list={list} />
                            );
                        })}
                    </div>
                </div>
            </>
        )
    }
}

export default HatsList
