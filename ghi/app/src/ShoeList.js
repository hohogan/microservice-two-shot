import React, { useState, useEffect } from 'react';



function ShoeList() {
    const [shoes, setShoes] = useState([])

    async function deleteShoe(event){
        const id=event.target.value;
        const binUrl = `http://localhost:8080/api/shoes/${id}`;
        const fetchConfig = {
            method:"delete"
        }
        const response = await fetch(binUrl, fetchConfig);

        if (response.ok){
            window.location.reload(true);
        }
    }

    async function fetchShoeList() {
        const response = await fetch('http://localhost:8080/api/shoes/');
        if (!response.ok){
            setShoes([]);
            return;
        }
        const shoeListData = await response.json();
        console.log(shoeListData);
        setShoes(shoeListData.shoes)
    }
    useEffect(() => {
        fetchShoeList();
    }, [])
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th> Color </th>
                    <th> Model Name </th>
                    <th> Manufacturer </th>
                    <th> Closet Name </th>
                </tr>
            </thead>
            <tbody>
                {shoes.map((shoe, index) => {
                    return (
                        <tr key={`${shoe.id}-${index}`}>
                            <td>{ shoe.color }</td>
                            <td> { shoe.model_name } </td>
                            <td> { shoe.manufacturer } </td>
                            <td> {shoe.bin.closet_name } </td>
                            <td><button onClick={deleteShoe} value={shoe.id}> Delete shoe </button> </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default ShoeList
