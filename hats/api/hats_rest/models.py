from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    closet_name = models.CharField(max_length=60)
    import_href = models.CharField(max_length=100, null=True)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.closet_name


class Hat(models.Model):
    name = models.CharField(max_length=60)
    fabric = models.CharField(max_length=60)
    style = models.CharField(max_length=60)
    color = models.CharField(max_length=60)
    url = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="location",
        on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("showHats", kwargs={"pk": self.pk})
