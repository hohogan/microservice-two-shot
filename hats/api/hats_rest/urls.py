from django.urls import path
from .views import listHats, showHats


urlpatterns = [
    path('', listHats, name="listHats"),
    path("<int:pk>/", showHats, name="showHats"),
]
